# class Persona
Person = Struct.new(:name, :age) do
 
  # attr_reader :name
  # attr_write :name, :age

  # attr_accessor :name, :age
  #
  # def initialize(name:, age:)
  #   @name = name
  #   @age = age
  # end

  # getter
  # def name
  #   @name
  # end

  # persona.name = raul | setter
  # def name=(name)
  #   @name = name
  # end
  
  def greet
    "hola, soy #{name}"
  end
end

# persona = Persona.new(name: "Raul", age: 25)
persona = Person.new("Raul", 25)
puts persona.name
puts persona.greet
