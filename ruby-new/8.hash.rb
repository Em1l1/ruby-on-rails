data = { "age" => 25, "name" => "raul" }
styles = { :font_size => 10, :font_family=>"Roboto"}


puts data["age"]
puts styles[:font_size]
puts styles[:font_family]

browsers = Hash.new

browsers["name"] = "Chrome"

puts browsers

puts browsers[:data]

browsers.default = "Not Found"
puts browsers[:data]

# https://ruby-doc.org/core-2.6/Hash.html
