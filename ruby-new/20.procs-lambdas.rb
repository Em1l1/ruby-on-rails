# def run_proc_with_random_number(&proc)
#   proc.call(rand(10))
# end

# proc = Proc.new { |number| puts "#{number}" }
# run_proc_with_random_number(proc)

# run_proc_with_random_number { |number| puts "#{number}" }
# run_proc_with_random_number(proc)

def return_from_proc
  a = Proc.new { return 10 }.call
  puts 'This will never be printed'
end

def return_from_lambda
  a = -> { return 10 }.call
  puts "The lambda returned #{a}"
end

# puts return_from_proc
return_from_lambda
