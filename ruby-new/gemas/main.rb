# RubyGems: https://rubygems.org/
# RubyToolbox: https://www.ruby-toolbox.com/
require 'rest-client'

# response = RestClient.get('https://jsonplaceholder.typicode.com/todos/1')
#
# puts response.code
# puts response.body
class MyRestClient
  def get(url)
    RestClient.get(url)
  end
end
