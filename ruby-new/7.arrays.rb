array = [1, "Two", 3.0]
array = %w{a b c d e f g h}

array = Array.new
Array.new(3)
Array.new(3, "hola")

# link ruby: https://ruby-doc.org/core-2.6/Array.html
#
# irb(main):001:0> array = %w{a b c d e f g}
# => ["a", "b", "c", "d", "e", "f", "g"]
# irb(main):002:0> Array.new(3)
# => [nil, nil, nil]
# irb(main):003:0> Array.new(3, "hello")
# => ["hello", "hello", "hello"]
# irb(main):004:0> array
# => ["a", "b", "c", "d", "e", "f", "g"]
# irb(main):005:0> array[2]
# => "c"
# irb(main):006:0> array[1, 4]
# => ["b", "c", "d", "e"]
# irb(main):007:0> array[1..4]
# => ["b", "c", "d", "e"]
# irb(main):008:0> array[1...4]
# => ["b", "c", "d"]
# irb(main):009:0> array[-2]
# => "f"
# irb(main):010:0> array.at(3)
# => "d"
# irb(main):011:0>
# irb(main):011:0> array.first
# => "a"
# irb(main):012:0> array.last
# => "g"
# irb(main):014:0> array.take(3)
# => ["a", "b", "c"]
# irb(main):015:0> array.drop(2)
# => ["c", "d", "e", "f", "g"]
#irb(main):016:0> browsers = %w{firefox chrome brave epiphany falkon}
# => ["firefox", "chrome", "brave", "epiphany", "falkon"]
# irb(main):017:0> browsers.length
# => 5
# irb(main):018:0> browsers.count
# => 5
# irb(main):020:0> browsers.empty?
# => false
# irb(main):021:0> browsers.include?('Edge')
# => false
#
# Agregar nuevos elementos al Array
# irb(main):022:0> browsers.push('Edge')
# => ["firefox", "chrome", "brave", "epiphany", "falkon", "Edge"]
# irb(main):023:0> browsers << 'IExplore'
# => ["firefox", "chrome", "brave", "epiphany", "falkon", "Edge", "IExplore"]
# irb(main):024:0> browsers.unshift('tree')
# => ["tree", "firefox", "chrome", "brave", "epiphany", "falkon", "Edge", "IExplor
# e"]
# irb(main):031:0> browsers.insert(2, "Canary", "Onion")
# =>
# ["tree",
#  "firefox",
#  "Canary",
#  "Onion",
#  "chrome",
#  "brave",
#  "epiphany",
#  "falkon",
#  "Edge",
#  "IExplore"]
#
# Eliminar elemento
#
# irb(main):032:0> browsers.pop
# => "IExplore"
# irb(main):033:0> browsers.shift
# => "tree"
# irb(main):034:0> browsers
# => ["firefox", "Canary", "Onion", "chrome", "brave", "epiphany", "falkon", "Edge
# "]
# irb(main):035:0> browsers.delete_at(1)
# => "Canary"
# irb(main):036:0> browsers.delete("chrome")
# => "chrome"
# irb(main):037:0> browsers << "Chrome"
# => ["firefox", "Onion", "brave", "epiphany", "falkon", "Edge", "Chrome"]
# irb(main):038:0> browsers.uniq
# => ["firefox", "Onion", "brave", "epiphany", "falkon", "Edge", "Chrome"]
# irb(main):039:0> browsers << "Chrome"
# => ["firefox", "Onion", "brave", "epiphany", "falkon", "Edge", "Chrome", "Chrome
# "]
# irb(main):040:0> browsers.uniq
# => ["firefox", "Onion", "brave", "epiphany", "falkon", "Edge", "Chrome"]
# irb(main):041:0> browsers.uniq!
# => ["firefox", "Onion", "brave", "epiphany", "falkon", "Edge", "Chrome"]
# irb(main):042:0> browsers
# => ["firefox", "Onion", "brave", "epiphany", "falkon", "Edge", "Chrome"]
# irb(main):043:0>
#

