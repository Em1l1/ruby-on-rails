# Condicionales 
=begin
== igual
!= distinto
>= mayor o igual que
<= menor o igual que
> mayor que
< menor que
=end

# condicional if
is_autenticated = false

if is_autenticated
  puts 'redirect to dashboard'
else
  puts 'redirect to login'
end


# condicional while
# role = :user

role = :super_user

case role
when :user
  puts "Welcome user"
when :super_user
  puts "Welcome SUPERUSER"
else
  puts "Error: usuario no reconocido"
end


# condicional unless
username = 'Pepe'

unless username == 'Pepe'
  puts "Eres un gran piloto #{username}"
end

conditionalVar = 10
firstInline = unless conditionalVar < 0 then puts 'Lower than 10' end
secondInLine = unless conditionalVar < 0
  puts 'Lower than 10 X2'
end
