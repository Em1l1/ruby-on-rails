<h1>Ruby</h1>

<h3>Simón Soriano</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Lo que aprenderás sobre Ruby](#lo-que-aprenderás-sobre-ruby)
  - [¿Qué es Ruby? Ventajas, desventajas y ¿quiénes lo están usando?](#qué-es-ruby-ventajas-desventajas-y-quiénes-lo-están-usando)
  - [Instalación de Ruby](#instalación-de-ruby)
- [2. Bases del lenguaje](#2-bases-del-lenguaje)
  - [Tipos de datos I: Integer, float](#tipos-de-datos-i-integer-float)
  - [Tipos de datos II: Strings](#tipos-de-datos-ii-strings)
  - [Tipos de datos III: Symbols](#tipos-de-datos-iii-symbols)
  - [Tipos de datos IV: Arrays](#tipos-de-datos-iv-arrays)
  - [Tipos de datos V: Hashes](#tipos-de-datos-v-hashes)
  - [Uso de condicionales](#uso-de-condicionales)
  - [Uso de ciclos](#uso-de-ciclos)
  - [Rangos](#rangos)
  - [Uso de Regex](#uso-de-regex)
  - [Procs y lambdas](#procs-y-lambdas)
  - [Programación Orientada a Objetos en Ruby Comenzando la creación de nuestra clase](#programación-orientada-a-objetos-en-ruby-comenzando-la-creación-de-nuestra-clase)
  - [Programación Orientada a Objetos en Ruby: Módulos](#programación-orientada-a-objetos-en-ruby-módulos)
  - [Programación Orientada a Objetos: Clases y Objetos](#programación-orientada-a-objetos-clases-y-objetos)
  - [Concurrencia vs Paralelismo: Threads en Ruby](#concurrencia-vs-paralelismo-threads-en-ruby)
  - [Bundler y gemas](#bundler-y-gemas)
  - [Testing en Ruby](#testing-en-ruby)
  - [Testing con MiniTest](#testing-con-minitest)
  - [Diferencias entre Ruby 2.5 y 3](#diferencias-entre-ruby-25-y-3)
- [3. Proyecto](#3-proyecto)
  - [Introducción al proyecto](#introducción-al-proyecto)
  - [Instalar Ruby 2D](#instalar-ruby-2d)
  - [Overview Ruby 2D](#overview-ruby-2d)
  - [Arquitectura](#arquitectura)
  - [Estructurando el proyecto](#estructurando-el-proyecto)
  - [Vista básica](#vista-básica)
  - [Programando la base para los movimientos](#programando-la-base-para-los-movimientos)
  - [Programando el movimiento de nuestro snake](#programando-el-movimiento-de-nuestro-snake)
  - [Test de nuestras acciones](#test-de-nuestras-acciones)
  - [Implementando el timer del movimiento](#implementando-el-timer-del-movimiento)
  - [Actualizando la vista](#actualizando-la-vista)
  - [Preparando el cambio de dirección](#preparando-el-cambio-de-dirección)
  - [Recapitulación de la arquitectura](#recapitulación-de-la-arquitectura)
  - [Moviendo la serpiente con el teclado](#moviendo-la-serpiente-con-el-teclado)
  - [Creciendo la serpiente y programando la condición de derrota](#creciendo-la-serpiente-y-programando-la-condición-de-derrota)
  - [Generando comida aleatoria: uso de rand y stub en pruebas](#generando-comida-aleatoria-uso-de-rand-y-stub-en-pruebas)
  - [Condición de salida y conclusiones](#condición-de-salida-y-conclusiones)
  - [Retos del curso](#retos-del-curso)

# 1. Introducción

## Lo que aprenderás sobre Ruby

Learnig Ruby is a very interesting topic. It's a language 
that is used to develop web applications.

## ¿Qué es Ruby? Ventajas, desventajas y ¿quiénes lo están usando?

Ruby es un lenguaje de programación creado por Matz en 1995, es open source y fue popularizado por Rails en 2005. Este lenguaje es dinámico, interpretado y orientado a objetos (todo es un objeto). Su objetivo es la felicidad y productividad del desarrollador.

**Ventajas**

Comunidad muy grande
Muchas librerías
Constantes actualizaciones
Lenguaje maduro
Desventajas

Interpretado puede significar lento
Alto uso de memoria
No paralelismo
Ha venido decreciendo en popularidad

- Este lenguaje es **dinámico** (Las variables no están asociadas a un tipo de dato especifico sino que pueden ir cambiando a medida que el programa se va desarrollando).
- Este lenguaje es **interpretado** (En Ruby necesitamos de un programa adicional llamado El interprete que va a leer nuestro archivo de código Ruby y va a ejecutar linea por linea sin crear un binario).
- Este lenguaje es **orientado a objetos** (todo es un objeto).
- Su objetivo es **la felicidad y productividad del desarrollador** (Esto se ve cuando escribimos o leemos una linea de código en Ruby ya que es muy parecida al ingles lo que nos permite poder entender facilmente las lineas de código pues es muy parecida al lenguaje humano).


- [mruby](https://mruby.org/)
- [jruby](https://jruby.org/)
- [Ruby](https://www.ruby-lang.org/en/documentation/ruby-from-other-languages/to-ruby-from-c-and-cpp/)

## Instalación de Ruby

Instalacion de Ruby para Linux:

**- apt (Debian o Ubuntu)**

```sh
$ sudo apt-get install ruby-full
```

**- yum (CentOS, Fedora, or RHEL)**

```sh
$ sudo yum install ruby
```

**- portage (Gentoo)**

```sh
$ sudo emerge dev-lang/ruby
```

**- pacman (Arch Linux)**

```sh
$ sudo pacman -S ruby
```

**- Homebrew (macOS)**

```sh
$ brew install ruby
```

Puedes usar:

```sh
ruby --version
```

[![img](https://www.google.com/s2/favicons?domain=https://www.ruby-lang.org/es/documentation/installation//favicon.ico)Instalar Ruby](https://www.ruby-lang.org/es/documentation/installation/)

# 2. Bases del lenguaje

## Tipos de datos I: Integer, float

Las **variables** son espacios reservados en la memoria que, como su nombre indica, pueden cambiar de contenido a lo largo de la ejecución de un programa.

Para declarar una variable escribimos el nombre de la variable, utilizar el operador igual “”="" y luego el valor que será asignado a esa variable.

Existen diferentes tipos de datos: el **Integer** o entero que puede representar un subconjunto finito de los números enteros y el **Float** o real que es la representación de un real estándar.

Los tipos de datos admitidos por Ruby incluyen números enteros y flotantes, cadenas, rangos, símbolos y booleanos, y nulos, así como dos estructuras de datos importantes: Array y Hash.

Conceptos extraídos de:

- http://www.w3big.com/ruby/ruby-datatypes.html

## Tipos de datos II: Strings

Algunos métodos para un string:

- “hola”.upcase (Devuelve una copia de la cadena en mayúsculas)
- “hola”.downcase ( Devuelve una copia de la cadena en minusculas)
- “hola”.length (Devuelve la cantidad de caracteres de la cadena)
- “HoLa”.swapcase (Devuelve una copia de la cadena con caracteres alfabéticos en mayúsculas convertidos a minúsculas y viceversa)
- “hola”.include? “h” (Devuelve true si el carácter asignado entre está incluido en nuestra cadena o variable)
- " hola".strip (Devuelve una copia de la cadena con caracteres alfabéticos en mayúsculas convertidos a minúsculas y minúsculas convertidos a mayúsculas)
- “”.empty (Devuelve true si la cadena está vacia)
- “Hola ruby”.gsup(“ruby”, “mundo”) ( Devuelve una copia de la cadena con todas las apariciones de patrón sustituidas por el segundo argumento )
- gsub! (Para modificar la variable en ves de devolver una copia)

> Cuando un método lleva signos de admiración ** ! ** por lo general se le conoce como **Método Bang**

**Métodos para strings**

Las cadenas en Ruby tienen muchos métodos integrados que facilitan la modificación y manipulación del texto, ya que en la mayoría de casos este es tratado como un objeto. Algunas de las cosas que podemos hacer son:

- Acceso a caracteres dentro de una cadena
- Convertir a mayúsculas o minúsculas, capitalizar una cadena
- Relleno o eliminación de caracteres en una cadena
- Búsqueda de caracteres y texto
- Sustitución de texto en cadenas
- Determinar la longitud de la cadena
- Interpolar cadenas de texto

Conceptos extraídos de :

- https://www.digitalocean.com/community/tutorials/how-to-work-with-string-methods-in-ruby
- https://www.rubyguides.com/2018/01/ruby-string-methods/

En Ruby hay diferentes formas de declarar un string

```ruby
saludo = "Hola"
saludo = 'Hola'
# Estas notaciones nos ayudan a cuando necesitamos utilizar comillas dentro de un string like = %q(María "la patilla" Coll)
saludo = %q(Hola)
saludo = %Q(Hola)
```

Otra característica útil es que podemos reemplazar (interpolar) una variable dentro de la declaración de un string

```ruby
nombre = maría
saludo = "Hola #{nombre}"
saludo = %Q(Hola #{nombre})

#Si lo hacemos con comillas sencillas desactivamos la interpolación
saludo = 'Hola #{4 + 5}'
```

Podemos escribir string sin que sean variables

```ruby
"Pepe".class

# Para poner todo uppercase
"Pepe".upcase

# Para poner todo lowercase
"PEPE".downcase

# Para saber de que tamaño es un string
"Pepe".length

# Para invertir el casing de cada uno de los carácteres
"PePE".swapcase # pEpe

# Para preguntar si una variable contiene x carácter
"Pepe".include? "a"

# Si tenemos un string que tenga espacio innecesarios en el inicio y el final para que lo quite utilizar el método
"   Pepe   ".strip

# Podemos preguntar si un string está vacío
"Pepe".empty?

# Para reemplazar algo dentro del string
"Pepe".gsub("Pe", "Ru")
```

Podemos utilizar los operadores para buscar cosas o sumar.

```ruby
# Para concatenar un string
"Pepe" + " simon"

# Podemos multiplicar un string
"Pepe"*3

# Para crear una nueva variable con gsub
nombre_2 = "Pepe Díaz".gsub("Díaz", "Fernandez")

# Pero para cambiar el valor de una variable
"Pepe Díaz".gsub!("Díaz", "Fernandez")
```

Normalmente si un método tiene un ***’!'\*** es por este va a modificar una variable.

## Tipos de datos III: Symbols

Los **symbols** son un tipo de dato especial que utiliza Ruby para declarar constantes, esto existe para evitar que se generen más objetos y por lo tanto, no generar espacio de memoria adicional.

Para declarar un símbolo se utilizan los dos puntos “”:"", ejemplo:
`color = :rojo`

**Símbolos**

Un símbolo es el objeto más básico que puedes crear en Ruby: es un nombre y una ID interna. Los símbolos son útiles por que dado un símbolo, se refiere al mismo objeto en todo el programa. Por lo tanto, son más eficientes que los strings: dos strings con el mismo nombre, son dos objetos distintos. Esto implica ahorrar de tiempo y memoria.

```ruby
    + Un símbolo parece una variable, pero está precedido de dos puntos. Ej: :action,
    :line_tines
    + Los símbolos no contienen valores como las variables
    + Es una cadena inmutable, osea que no se puede modificar en tiempo de ejecución.
    + Al ser inmutables, no se les puede pasar métodos
    + Cuando creo dos símbolos con el mismo valor, Ruby reciclara segundo, lo que genera mayor performance
    + Se recomienda su uso cuando no se necesita modificar el “strign”, cuando no necesito los métodos del “strign”, cuando necesito utilizar un nombre (como las claves de los hashes)
```

Cada vez que se ha usado un string, se ha creado un objeto nuevo. Por tanto, ¿cuándo usar un string, y cuándo un símbolo?

- Si el contenido del objeto es lo importante, usa un string.
- Si la identidad del objeto es importante, usa un símbolo.
  Al contrario que las cadenas (strings), los símbolos con el mismo nombre son inicializados y existen en memoria una sola vez en cada sesión de Ruby.

Conceptos extraídos de :

- http://rubytutorial.wikidot.com/simbolos
- http://www.railsenespanol.co/capitulos/capitulo1



## Tipos de datos IV: Arrays

Un **array** o arreglo es un tipo de datos especial donde vas a poder almacenar múltiples objetos en una sola variable.

Ejemplo:

```ruby
letras = [ ""q"", ""w"", ""e"", ""r"", ""t"", ""y""]
```

*Algo interesante de Ruby es que podemos utilizar números negativos para acceder a las posiciones desde el último elemento del array.*

Los arreglos en Ruby no tienen un tipo específico, esto quiere decir que dentro de un array podemos tener diferentes tipos de datos en cada elemento.

Métodos:

- letras.include? “y” (Devuelve true si incluye dicho elemento)
- letras.count { |x| x == “y” } (Nos retorna la cantidad de elementos que cumplen dicha condición)
- [1,2,3,4,5].map { |x| x*2 } (Nos retorna un arreglo modificado según la condición)
- [1,2,3,4,5].select { |x| x.even? } (Nos retorna un arreglo con los elementos seleccionados según la condición)
- [1,2,3,4,5].min (Nos va a retornar el elemento más pequeño)
- [1,2,3,4,5].max (Nos va a retornar el elemento más grande)
- [1,2,3,4,5].sum (Retorna la suma de todos los elementos del arreglo)
- “Hola Mundo”.split(" ") (Nos retorna un arreglo con la cadena dentro separada por el elemento dentro del paréntesis)
- [“a”, “b” ,“c”].join("-") (Retorna los elementos de una matriz en una cadena separada por el elemento dentro del paréntesis)
- letras.sort (Nos retorna una copia ordenada del arreglo)

## Arrays

------

Un Array es un tipo de dato especial donde vamos a poder almacenar múltiples objectos en una sola varibles

```ruby
# Así escribimos un array
letras = ["a","b","c","d","e","f","g"]

# Para acceder a las posición de cada objeto, lo hacemos como en JS
letras[0]
```

Algo especial de Ruby es que podemos utilizar números negativos para acceder a posiciones desde el último elemento en el Array. Si utilizamos un número que exceda la cantidad de objetos que hay en el Array no mostrar ningún resultado.

```ruby
letras[-1]
```

También podemos acceder al método **methods** con métodos

```ruby
# Para ver que clases de objeto es methods
letras.methods.class

# Para cuánto objetos hay dentro de methods
letras.methods.size
```

Otros métodos que podemos utilizar para un Array son:

```ruby
# Para saber si hay un elemento x dentro del array
letras.include? "a"

# Para saber cuál es el primer elemento
letras.first

# Para saber cuál es el último elemento
letras.last
```

Con el método **.count** podemos especificar una condición, es como una mini función, recibe un parámetro y podemos preguntar algo sobre el parámetro

```ruby
# Estamos preguntando cuántos elementos son iguales a "q"
letras.count { |x| x == "q" }

# Estamos preguntando cuántos elementos son pares
[1,2,3,4,5].count { |x| x.even? }

# El método .map se utiliza para transformar cada uno de los elementos del array
[1,2,3,4,5].map { |x| x*2 }

# El método .select es equivalente al método .filter de JS. Se encarga de filtrar algunos elementos que cumplan con una condición
[1,2,3,4,5].select { |x| x.odd? }

# Nos retorna el elemento más pequeño
[1,2,3,4,5].min

# Nos retorna el elemento más grande
[1,2,3,4,5].max

# Va sumar todos los elementos del Array
[1,2,3,4,5].sum
```

Algo a tener en cuenta es que los Arrays en Ruby no tienen un tipo es específico, lo cual quiere decir que pueden ser de diferentes tipos.

```ruby
array_1 = [1, 4.5, "string", :symbol, []]

array_1.map { |x| x.class }
```

Algunos **Strings** retornar Arrays.

```ruby
"Hola mundo".split(" ") # ["Hola","mundo"]

"Hola mundo".split(" ").size # 2

"Hola mundo".split(" ").map { |x| x.upcase } # ["HOLA", "MUNDO"]

puts("Hola mundo".split(" ").map { |x| x.upcase }.join("\n") )
# HOLA 
# MUNDO

# El siguiente método puede ordenar el Array
["r","i","c","w","e","f","a"].sort # ["c", "i", "r", "w"]
```

## Tipos de datos V: Hashes

**Hashes**, también conocidos como arrays asociativos, mapas o diccionarios, son parecidos a los arrays en que son una colección indexada de referencias a objetos. Sin embargo, mientras que en los arrays los índices son números, en los hashes se puede indexar con objetos de cualquier tipo.

Cuando se almacena un valor en un array, se dan dos objetos: el índice y el valor. Luego, se puede obtener dicho valor, gracias al índice.

**Hashes**

Es una estructura que almacena datos como un diccionario lo haría (valor -> 10. Información asociada a ese valor)

- Se definen dentro de llaves

  - ej: 

    ```ruby
    estudiante = {“name” => “victor”, [] => “Soy arreglo”, “Ciudad” => “Bogotá”, 30 => “Años”}
    ```

    

- Se le pueden asignar valores por defecto a las posiciones vacías

- Pueden modificarse en tiempo de ejecución

- Para los valores clave por regla suele utilizarse un símbolo

  - ej: 

    ```ruby
    estudiante = {:name => “victor”, :Ciudad => “Bogotá”, :30 => “Años”}
    ```

- Si solo hay claves “símbolos” por regla se escribe asi

  ```ruby
  ej: estudiante = {name: “victor” Ciudad: “Bogotá” 30: “Años”}
  ```

  

- En otros lenguajes son comúnmente conocidos como arreglos asociativos

- En un arreglo se accede a la informacion por medio de un indice, en los hashes la clave con la que podemos acceder pude ser cualquier tipo de objeto, incluso un símbolo, o arreglo

- Conceptos extraídos de :

  - http://rubytutorial.wikidot.com/simbolos
  - https://makeitrealcamp.gitbook.io/ruby-book/hashes

## Uso de condicionales

En Ruby, nil y false significan falso, todo lo demás (incluyendo true, 0) significan verdadero. En Ruby, nil es un objeto: por tanto, tiene sus métodos, y lo que es más, puedes añadir los métodos que se quieran.

**Métodos condicionales**

- if -> Se ejecuta si se cumple la condición
- elsif -> Se ejecuta si la condición “if” no se cumple, y se pueden anidar tantos como sean necesarios
- else -> Se ejecuta si no se cumplen las funciones antes descritas
- case -> Esta instrucción es muy parecida al if: se crean una serie de condiciones, y se ejecuta la primera condición que se cumpla.
- unless -> Ruby tiene una forma contraria al if: la instrucción unless. Y digo contraria, por que en if se ejecutaba el bloque (do … end) si se cumplía la condición; con unless se ejecutará el bloque mientras NO se cumpla.

**Operadores de comparacion**

- == igual

- != distinto

- > = mayor o igual que

- <= menor o igual que

- > mayor que

- < menor que

Conceptos extraídos de :

- http://rubytutorial.wikidot.com/condiciones-loops
  -https://makeitrealcamp.gitbook.io/ruby-book/condicionales

## Uso de ciclos

Los ciclos nos permiten repetir la ejecución de un código las veces que consideremos necesarias, podemos utilizar cualquiera de los siguientes.

- While: Ejecuta código mientras condicional es verdadero
- until: Ejecuta código mientras condicional es falso
- for: Ejecuta el código una vez para cada elemento en la expresión
- each: Ejecuta el código una vez para cada elemento en la expresión, sin crear un nuevo ámbito para las variables locales
- times: Ejecuta el código la cantidad de veces estipulada en la variable que condiciona “times”
- break: Termina el bucle más interno.
- next: Salta a la siguiente iteración del bucle más interno
- redo: Reinicia esta iteración del bucle más interno, sin verificar la condición del bucle
- retry: Se ejecuta de nuevo la sección del código que se rescató.

```ruby
# Se loopea mientras la condición sea verdadera

secret_word="estebandido"

guess=""
guess_count=0
guess_limit=3
out_of_guesses=false

#mientras la palabra a adivinar sea difenrete de la palabra secreta y no estes outofguesses
while guess != secret_word and !out_of_guesses

  if guess_count<guess_limit
  puts "Enter guess"

      guess = gets.chomp()
      guess_count+=1
    else
      out_of_guesses=true

    end
end

if out_of_guesses
  puts "looser"
else
  puts "you`re a master"
end
```

Crear iteraciones o repeticiones en Ruby es muy parecido a como se hace en otros programas, en el siguiente código vamos a declarar un ciclo de **while**

```ruby
#básica
while condicion do
    puts "Hola"
end

#Ejemplo de la clase
x = 1

while x < 5 do
    puts "Hola #{x}"
    x +=1
end
```

Para que el resultado sea menor y igual a 5 tenemos que poner ***<=\***

```ruby
while x <= 5 do
    puts "Hola #{x}"
    x +=1
end
```

Cuando declaramos un **loop** se convertirá en un ciclo infinito, así que nosotros explícitamente tenemos que decirle cuando queremos que termine

```ruby
loop do
    puts "Hola #{x}"
    break if x >= 5
    x +=1
end
```

Y para declarar una **for**, lo haremos de la siguiente manera

```ruby
for i in 1..5 do
    puts "Hola #{i}"
end
```

También podemos hacer una iteración es creando un Array y pasandole el método .each

```ruby
[1,2,3,4,5].each { |x| puts "Hola #{x}" }
```

Y por último podemos crear un ciclo con el método .times, aunque este comienza en 0

```ruby
5.times { |x| puts "Hola #{x}" }
```

------

## Shortcuts

ctrl+} o ctrl+alt+a , Para comentar las líneas de código que están seleccionadas en VSC

Conceptos extraídos de :
\- https://makeitrealcamp.gitbook.io/ruby-book/ciclos
\- http://rubysur.org/aprende.a.programar/capitulos/condicionales.html
\- https://www.tutorialspoint.com/ruby/ruby_loops.htm
\- https://blog.honeybadger.io/how-to-try-again-when-exceptions-happen-in-rubyi

## Rangos

Rangos, el principal uso y quizás el más apropiado para los rangos, es expresar una secuencia: las secuencias tienen un punto inicial y un punto final, y una forma de producir los sucesivos valores entre ambos. En Ruby, esas secuencias son creadas usando los operandos ‘…’ y ‘…’

‘…’ genera una secuencia donde los puntos límites están incluidos.

```ruby
(1..3).to_a

#es la secuencia 1, 2, 3
```


‘…’ genera una secuencia en la que no está incluida el límite superior.

```ruby
(1...5).to_a

#equivale a 1, 2, 3, 4
```

## Uso de Regex

Regex es un tema amplio y complejo, es por esto que en Platzi hemos creado un curso enfocado 100% en Expresiones Regulares, te invitamos a tomarlo.

Para declarar un regex, se utiliza el “”/"" , ejemplo:

```ruby
is_gmail_regex = /\w+@gmail.com/
```

Una expresión regular viene a ser un patrón, que se ajusta a un string para que coincida o no, son una poderosa herramienta para trabajar con texto.

Un regex se declara entre slash “/”
Un regex puede tener multiples matches

Algunas abreviaciones para clases de caracteres

Todas las abreviaciones precedentes, también tienen una forma negada. Para ello, se pone la misma letra en mayúsculas:

| Expresión | significado                                                  |
| --------- | ------------------------------------------------------------ |
| .         | cualquier carácter                                           |
| []        | especificación por rango. P.ej: [a-z], una letra de la a, a la z |
| \w        | letra o número; es lo mismo que [0-9A-Za-z]                  |
| \W        | cualquier carácter que no sea letra o número                 |
| \s        | carácter de espacio; es lo mismo que [ \t\n\r\f]             |
| \S        | cualquier carácter que no sea de espacio                     |
| \d        | número; lo mismo que [0-9]                                   |
| \D        | cualquier carácter que no sea un número                      |
| \b        | retroceso (0x08), si está dentro de un rango                 |
| \b        | límite de palabra, si NO está dentro de un rango             |
| \B        | no límite de palabra                                         |
| *         | cero o más repeticiones de lo que le precede                 |
| +         | una o más repeticiones de lo que le precede                  |
| $         | fin de la línea                                              |
| {m,n}     | como menos m, y como mucho n repeticiones de lo que le precede |
| ?         | al menos una repetición de lo que le precede; lo mismo que {0,1} |
| ()        | agrupar expresiones                                          |
| pipeline* | operador lógico O, busca lo de antes o lo después            |

- un pipeline es el carácter " | "
  - Conceptos extraídos de :
    - http://rubytutorial.wikidot.com/expresion
    - https://carlossanchezperez.wordpress.com/2013/06/30/expresiones-regulares-en-ruby-no-te-dejes-intimidar-primera-parte/

## Procs y lambdas

- Un bloque no es un objeto, es parte de la sintaxis de ejecución de un método, por tanto no tiene las funcionalidades de un objeto.
- Podemos simular la funcionalidad de un objetos al pasar un bloque como un “proc”, la sintaxis dice que antes del nombre de la variable de método se bebe utilizar el signo “&” el cual le indica a ruby que el bloque que se esta pasando en realidad se trata de un objeto “proc”
- Un método solo puede recibir un bloque, pero puede recibir varios “proc”
- Es las lenta la ejecución de un proc que la de un bloque

Procedimiento return en un proc

Siempre que intentemos utilizar la palabra reservada **return** en un proc debemos tener en cuenta que :

- Para un proc se debe llamar un metodo y dentro del método declarar el procedimiento que a utilizar.
- Si no lo hacemos de este modo nos retornara Error “local jump error” porque esta tratando de ejecutar un return de primer nivel

**Lambda**

- Una “lambda” es una función anónima (sin nombre), su sintaxis es “lambda {…}” o también utilizando la sintaxis de flecha “->(){}”
- Hay similitudes muy grandes entre los “procs” y las “lambdas”, lo que los diferencia es que un “proc” se comporta como un bloque, mientras que una “lambda” se comporta como un método
- A una “lambda” se le pueden pasar argumentos, pero al igual que a un metodo estos deben pasarse en el mismo orden que fueron llamados y son obligatorios, lo que no sucede en un “proc” pues para este es opcional
- Cuando se hace “return” de una desde una “lambda” se detiene la ejecución del método (lambda), pero no se detiene la ejecución de donde fue llamado (otro método), caso contrario si estamos trabajando con bloques que si termina la ejecucion del metodo donde fue llamado

Conceptos extraídos de:

- http://nereida.deioc.ull.es/~lpp/perlexamples/node154.html
- http://community.logicalbricks.com/node/111ssss

[Este vídeo lo explica perfectly](https://www.youtube.com/watch?v=b8pogbgrZ1o)
Este es el código que verán el vídeo más un pequeños apuntes:

```ruby
# Procs

# Los procs se comportan como bloques. Se recomienda pasar el Proc de forma explícita

def run_proc_w_random_number(proc)
    proc.call(rand(100))
end

proc = Proc.new { |number| puts "#{number}" }
run_proc_w_random_number(proc)

# No tan buen en performace la siguiente, pero no se recomienda

def run_proc_w_random_number(&proc)
    proc.call(rand(100))
end

run_proc_w_random_number { |number| puts "#{number}" }

# Lambdas

# Las lambdas utiliza un return de la misma forma que hace un método

def return_from_proc
    a = Proc.new { return 10 }.call
    puts 'This will never be printed'
end

def return_from_lambda
    # a = lambda { return 10 }.call
    a = -> { return 10 }.call
    puts "The lambda returned #{a}"
end

return_from_proc
return_from_lambda
```

Ejercicio de la clase
En Ruby un Proc o lambda es una clase especial que hace una referencia a un método. Estos lambdas son utilizados para hacer programación funcional.

```ruby
# 1. Declarar un proc

saludar = Proc.new { |nombre| puts "Hola #{nombre}" }

# 2. LLamar a ese procedimiento

saludar.call("Carmen"
```

## Programación Orientada a Objetos en Ruby Comenzando la creación de nuestra clase

Reduciendo la definición de una clase:
Forma 1:

```ruby
class Persona  
  def initialize(name) #Constructor
    @name = name #Variable de instancia se inicializa con el parametro en el constructor
  end

  def name #Getter
    @name
  end

  def name=(name) #Setter
     @name = name
     self
  end
end
```

Forma 2:

```ruby
class Persona
  attr_accessor(:name)  # Genera getter y setter

  def initialize(name)
    @name = name
  end
end
```

Forma 3:

```ruby
class Persona < Struct.new(:name) #Struct declara el constructor, getters y setters
end
```

Forma 4 en una linea:

```ruby
Persona = Struct.new(:name)
```

**POO en Ruby**

- Un objeto es un a colección de propiedades y métodos.
- Las propiedades de un objeto son valores que están asociados a un objeto
- En ruby generalmente las propiedades son llamadas variables de instancia,
  en código estas variables se distinguen por llevar un “@” al inicializar
  el nombre de la variable ej: @mi_nombre
- Variables de instancia son identificadores que le pertenecen al objeto,
  no a la clase por eso se llaman de “instancia”
- Estas variables no están disponibles para (leer / modificar) fuera de la definición de la clase, para ello se deben utilizar métodos accesores
- Los métodos accesores son de dos tipos, para leer y para escribir; aunque en ruby
  no se identifican de la forma que se hace en otros programas con "SET/GET"
  sino que y en ruby (geters/seters) se ven iguales al nombre de la propiedad y la
  forma como se distinguen el uno del otro es que el método accesor “set” tiene el
  operador igual “=”.
- Hay métodos específicos para acceder a estas funcionalidades como (attr_accessor, attr_reader y attr_writer) y el método attr_accesor que realiza simultáneamente las funciones de escritura y lectura.

## Programación Orientada a Objetos en Ruby: Módulos

Una buena práctica de programación es la modularización del código,  lo que algunos llaman alta cohesión, lo que quiere decir que las clases o funcionalidades que hacen cosas similares estén dentro del mismo contenedor; en Ruby se utilizan los módulos para ello.

Es una porcion de codigo que nos permite la funcionalidad de ser inyectado en una clase, se comporta como si tomara el código definido en su TestModuley lo insertara en su clase TestClass mediante el método include

```ruby
moduleTestModule
  defsome_method
    "Some method of #{self.class}"
  end
end

classTestClass
  include TestModule

  # ...

end

test = TestClass.new.some_method
puts test
```

Algunas de sus características son:

- No instancian objetos
- No crean clases derivadas
- se pueden utilizar para cualquier clase
- Los módulos de Ruby son similares a las clases porque contienen una colección de métodos, constantes y otras definiciones de módulos y clases.
- Los módulos se definen como las clases, pero la palabra clave de módulo se usa en lugar de la palabra clave de clase.
- Los módulos sirven como herramientas, es el equivalente a un mixin que se puede reutilizar a lo largo de las clases.
- Los módulos son agnósticos, los podemos utilizar en cualquier tipo de clase.

## Programación Orientada a Objetos: Clases y Objetos

- **Clase**

  → (Una abstracción) esta es compuesta de atributos y métodos.

  - **Atributos** → Son todas las propiedades que corresponden al jugador.
  - **Métodos** → Son todas las acciones que hará ese jugador.

- **Objeto** → Es la instancia de la clase.

Todo esto quiere decir que la clase será el molde de nuestros objectos, cada objeto que creemos tendrán características y a veces acciones diferentes.

Además podremos usar la **Herencia,** esta nos ayudará a crear clases a partir de otras. También podemos hacer un **encapsulamiento** de los atributos de nuestros objetos.

- **Polimorfismo** → Se refiere a la propiedad por la que es posible enviar mensajes sintácticamente iguales a objetos de tipos distintos.

Agregar o **Instancia** (o como se dice a veces, **instanciar**). Es decir la creación de un objeto a partir de una clase. En otras palabras, en **POO** un **objeto** es una **instancia** de una **clase**.

[Link a YouTube](https://www.youtube.com/watch?v=Mi_sRAfs7TE)

## Concurrencia vs Paralelismo: Threads en Ruby

### Concurrencia VS Paralelismo

Los términos concurrencia y paralelismo pueden ser fácilmente confundidos. Por un lado 2 tareas se ejecutan en paralelo cuando ambas se ejecutan en unidades de procesamiento independientes al mismo tiempo, es decir, ambas tareas pueden comenzar exactamente al mismo tiempo pues su ejecución es manejada por dos unidades de procesamiento diferente. Por otro lado, dos tareas se ejecutan concurrentemente cuando se pueden ejecutar en la misma unidad de procesamiento intercalando subtareas de ambas tareas.

Por ejemplo:

Hay una empresa que está desarrollando un blog como una aplicación web. Para esto necesita desarrollar el backend y el frontend para lo cual contrata a un desarrollador fullstack llamado Pepe que se puede encargar de ambas tareas. Pepe entonces tiene 2 tareas, hacer el backend y hacer el frontend, y a su vez estas 2 tareas se pueden dividir en pequeñas subtareas así:

- Backend
  + API para la autenticación
  + CRUD de posts

- Frontend
  - Vista de autenticación
  - Vista de los posts



Pepe puede trabajar en ambas tareas concurrentemente pues puede primero hacer una versión inicial de la implementación del API para la autenticación. Mientras su líder técnico revisa el código de esta versión inicial, Pepe puede comenzar a implementar la vista de autenticación y una vez su líder técnico termine la revisión de la versión inicial del API de autenticación, puede continuar con esta tarea y repetir el mismo proceso hasta terminar con todas las subtareas y tareas.

Si la empresa contrata a un desarrollador frontend y a un desarrollador backend, ambos desarrolladores pueden trabajar en ambas tareas en paralelo. Pues ambos pueden comenzar con ambas tareas al tiempo. En este ejemplo los desarrolladores son las unidades de procesamiento y el desarrollo del backend y frontend son las tareas que se pueden ejecutar en diferentes threads o unidades de procesamiento.
Ahora un ejemplo gráfico:

![Captura de pantalla 2018-12-06 a la(s) 13.37.11.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202018-12-06%20a%20la%28s%29%2013.37.11-cedb67a2-02aa-4f5a-b3f6-f7e7e7bf6a93.jpg)

Tomado de: https://joearms.github.io/published/2013-04-05-concurrent-and-parallel-programming.html

### Limitaciones de concurrencia en Ruby

En Ruby tenemos la posibilidad de crear Threads, sin embargo, su comportamiento depende del intérprete que utilicemos. El interprete que usamos en el curso que adicionalmente es el interprete más popular (cruby o MRI) no permite paralelismo asi el computador en el que ejecutemos nuestro programa tenga múltiples cores en su procesador. MRI utiliza un mecanismo llamado Global Interpreter Lock (GIL) que hace que el interprete solo pueda ejecutar un Thread a la vez. Esto es una decisión que tomaron quienes diseñaron el lenguaje pues es una manera relativamente sencilla de evitar race conditions, deadlocks y otros problemas comunes que surgen cuando se está haciendo programación concurrente o en paralelo.

Aunque el GIL no permite que multiples threads se ejecuten, sí permite cambiar de contexto cuando se esta realizando una operación por fuera del interprete como operaciones de lectura o escritura. Como estas operaciones suceden por fuera del interprete, cruby permite cambiar de contexto para ejecutar otro thread mientras estas operaciones terminan y de esta manera se puede hacer programacion concurrente.
Otros interpretes como JRuby y Rubinius no tienen un GIL así que permiten ejecución en paralelo.

### Threads en Ruby

![Captura de pantalla 2018-12-06 a la(s) 13.39.17.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202018-12-06%20a%20la%28s%29%2013.39.17-cd043c26-685a-43bb-ba9e-df70a681c1a2.jpg)

![Captura de pantalla 2018-12-06 a la(s) 13.41.06.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202018-12-06%20a%20la%28s%29%2013.41.06-acdfd80c-05c0-4b23-8702-7a54ad45a5c5.jpg)

En este ejemplo estamos simulando hacer 3 llamados HTTP. Cada llamado toma 3 segundos y estamos ejecutando estos llamados con y sin threads. Aunque el GIL no permite la ejecución en paralelo de múltiples threads, como la operación HTTP debe esperar un segundo por la respuesta (este comportamiento es simulado usando “sleep(1)”), el interprete puede cambiar de contexto y ejecutar los demás threads que necesiten ser ejecutados. De esta manera podemos ver que la ejecución sin threads toma 3 segundos pues ejecuta los 3 llamados en serie, sin embargo la ejecución que utiliza threads solo toma 1 segundo pues tan pronto un thread llama al método sleep, el interprete detecta que debe esperar así que puede cambiar de contexto y permitir la ejecución de los otros 2 threads.

### A tener en cuenta

Para inicializar un Thread se utiliza debe crear un objeto Thread con Thread.new y pasarle un bloque en donde definimos lo que se debe ejecutar.
El punto de entrada de un programa Ruby se ejecuta en un thread principal o “main thread”. Tan pronto este thread termina, la ejecución de todo el programa es terminado, así que si creamos varios threads pero el “main thread” finaliza primero los otros threads van a ser terminados. Para evitar esto debemos hacer “join” de los threads adicionales. Join es un mecanismo que hace que el thread principal espere a la finalizacion del thread al que se le hace join lo que podemos ver en la linea " threads.map(&:join)".

Referencias:
https://github.com/simon0191/platzi-curso-ruby/commit/8497e9ace3011767355fb814a473c39f896dfe67
https://www.toptal.com/ruby/ruby-concurrency-and-parallelism-a-practical-primer
https://joearms.github.io/published/2013-04-05-concurrent-and-parallel-programming.html
https://medium.com/@franzejr/ruby-3-mri-and-gil-a302577c6634
https://robots.thoughtbot.com/untangling-ruby-threads

## Bundler y gemas

Instalar la herramienta Bundler

```ruby
gem install bundler
```

See the version:

```sh
# version
bundle --version
```

Iniciar proyecto:

```sh
# crear directorio
take snake

# Iniciar proyecto
bundle init
```

Instalar faker

```ruby
# gem 'faker', '~> 1.9' utilizar una version en especial
```

Bundler nos permite gestionar las dependencias de librerias(gemas) para nuestros proyectos ruby

Para instalar bundler ejecutamos el comando

```sh
gem install bundler
```

Ahora disponemos del comando bundle. Con el comando bundle init generamos un archivo llamado Gemfile donde podemos definir las dependencias de gemas para nuestro proyecto ruby

Agregando gemas a Gemfile:

```ruby
gem 'faker'
# Es una buena practica especificar la version
gem 'faker', '~> 1.9'
```

Ahora es el momento de confirmar nuestras dependencias utilizando el comando
bundle install

Esta instruccion verifica las dependencias y procede a instalar las gemas. Tambien generará un archivo llamado Gemfile.lock que especifica las versiones de gemas utilizadas en nuestro proyecto

>  Ojo > el Faker de Harry potter ya cambio , la nueva forma de invocarlo es asi
> Faker::Movies::HarryPotter

Ejecutar `main.rb`

```sh
➜ bundle exec ruby main.rb 
```

¿Qué son las gemas de Ruby?
Son paquetes de librería para ruby, ayudan a ser más eficientes al crear código y además a ahorrar tiempo.

**Donde buscar gemas:**

- [Installing RVM | Ruby Version Manager](https://rvm.io/rvm/install)

-  [Installing Rubies](https://rvm.io/rubies/installing)

- [Find, install, and publish RubyGems.](https://rubygems.org)

- [ruby-toolbox](https://www.ruby-toolbox.com/)

## Testing en Ruby

El **testing** es una práctica de programación con la que podemos escribir código que va a probar el código de nuestra aplicación para garantizar que con cada cambio que le agreguemos al proyecto, no vamos a hacer que funcionalidades anteriores se vayan a ver afectadas por este nuevo cambio.

En Ruby existe una librería que nos ayuda con esto, se llama *MiniTest*

Significa que no tendrás que ejecutar tu programa y jugar con él (lo que es lento) para arreglar los errores. El testeo de unidades es mucho más rápido que el “testeo manual”.
Pueden ser


Las pruebas unitarias se escriben para probar la lógica de métodos específicos de la aplicación, aislando sistemas externos como bases de datos, otros servidores, etc.


Las pruebas de integración se escriben para probar funcionalidades completas de la aplicación y pueden involucrar componentes y sistemas externos como navegadores, bases de datos, frameworks, etc.


TDD es un proceso para escribir pruebas automatizadas en el que primero se escribe una prueba fallida, después se implementa el código para que funcione y, por último, se mejora el código verificando que la prueba siga pasando.


BDD es muy similar pero el lenguaje cambia un poco para que sea entendible tanto por programadores como por personas que tienen mayor conocimiento del negocio.


Librerías más populares

- [Minitest](https://github.com/seattlerb/minitest)
- [RSpec](http://rspec.info/)

>  No se olviden que para utilizar el require “byebug” se debe instalar la gema en gemfile, así:
>
> ```ruby
> gem ‘faker’, ‘~> 1.9’ and
> gem ‘byebug’, '~> 11.1.1’
> ```
>
> Luego le dan de nuevo en la consola :
>
> ```sh
> bundle install.
> ```
>
> Listo.

Conceptos extraídos de :

https://guias.makeitreal.camp/ruby-on-rails-ii/testing-en-ruby
https://blog.codeship.com/unit-testing-in-ruby/
http://rubytutorial.wikidot.com/unit-testing

## Testing con MiniTest

**Testing guías de estilo**

- Siga las convenciones sobre nombres y rutas para los archivos de test. Esto evita que tus pruebas entren en conflicto con tu otro código.
- Escribe un archivo de prueba para cada módulo que hagas.
- Mantenga sus casos de prueba (funciones) cortos.
- Aunque los casos de prueba son confusos, intente mantenerlos limpios y elimine cualquier código repetitivo
- Crear funciones de ayuda que deshacerse de código duplicado.
- El código duplicado hará que cambiar tus pruebas sea más difícil.
- A veces, la mejor manera de rediseñar algo es simplemente eliminarlo y volver a empezar. No te apegues demasiado a tus test.
- Aprende de metodologías

MiniTest es una librería de Ruby, ya viene incluida así que no es necesario instalarlo.
El autorun que está incluido en el require, se encargará de detectar automáticamente que dentro de nuestro archivo hay un test de Minitest, es cual lo ejecutará automáticamente sin necesidad de que explícitamente llamemos a ese test.
require "minitest/autorun"

```ruby
class Calculator
    def sum(a,b)
        a + b
    end

    def substract(a,b)
        a - b
    end

end

# "< Minitest::Test" Este le indica que esta es nuestra clase de pruebas

class TestCalculator < Minitest::Test
    # En este método que se ejecuta una sola vez antes de ejecutar todo el set de prueba.
    def setup
        @calc = Calculator.new
    end

# Aquí definimos los métodos con el prefijo 'test_'

def test_sum_positives
    result = @calc.sum(1, 3)
    assert_equal result, 4
end

def test_sum_negatives
    result = @calc.sum(-1, -3)

    # Este método está incluido en Minitest, aquí vamos aprobar si la condición que le pasamos va ha ser verdadera y en caso contrario va a fallar la prueba.

    assert_equal result, -4
end

end


```



>  UnitTest y MiniTest ya no forman parte del core de ruby, se tienen que instalar

## Diferencias entre Ruby 2.5 y 3

Gracias a que la comunidad de Ruby es muy activa, constantemente se están lanzando nuevas versiones del lenguaje y de los diferentes intérpretes. Al momento de crear este curso el último “release” es la versión 2.5.3. Para mantenerte actualizado sobre estas versiones puedes revisar la página oficial del lenguaje (https://www.ruby-lang.org) y también, cada vez que sale una nueva versión te recomiendo “googlear” los cambios que esta versión incluye.

**Por ejemplo:**

http://lmgtfy.com/?q=ruby+2.5+release+new+features en donde te encontraras con articulos como https://blog.jetbrains.com/ruby/2017/10/10-new-features-in-ruby-2-5/ o https://www.engineyard.com/blog/ruby-250-new-features en donde te resumen las nuevas caracteristicas que se incluyen en la nueva version. Adicionalmente cada nueva versión incluye parches de seguridad que son importantes para mantener tu aplicación siempre segura y protegida en contra de ataques por lo que es muy recomendable siempre estar revisando las nuevas versiones y actualizar las aplicaciones cuando sea posible.

**¿Qué esperar de Ruby 3?**

Los desarrolladores de Ruby tienen un proyecto llamado Ruby 3x3 con el que pretenden hacer que la versión 3 de Ruby sea 3 veces mas rápida que Ruby 2.
El termino rápido es muy subjetivo y depende de cómo se hagan las mediciones. Esto es algo que los desarrolladores del lenguaje tienen en cuenta así que no podemos esperar que con Ruby 3, nuestra aplicación web que utiliza Ruby on Rails sea 3 veces mas rápida que la versión que utiliza Ruby 2 pues el desempeño depende también de muchos detalles de implementación que están fuera del control de los desarrolladores del lenguaje.

Respecto al futuro de Ruby 3, los principales objetivos son:

- Desempeño
- Modelo de concurrencia
- Tipos

**Desempeño**

Por lo pronto el proyecto ha hecho varios avances que ya se pueden ver respecto a desempeño. Como el punto de comparación es Ruby 2.0, ya hemos podido ver varias mejoras entre los releases intermedios como Ruby 2.1, 2.2, 2.3, etc en donde se han hecho mejoras por ejemplo de manejo de memoria que hacen que el intérprete sea más rápido.

**Concurrencia**

Adicionalmente están trabajando en un nuevo modelo de concurrencia para ofrecer nuevas abstracciones que no solamente van a hacer posible ejecución en paralelo sino que además van a ofrecer una manera más fácil de crear programas concurrentes. Esta nueva abstracción es llamada guild, propuesta por Koichi Sasada en http://rubykaigi.org/2016/presentations/ko1.html. A grandes rasgos, los guilds son como grupos de ejecución de threads. Dentro del mismo guild pueden existir varios threads que comparten variables, pero estos threads en el mismo guild o grupo no se pueden ejecutar en paralelo. Sin embargo, threads en diferentes guilds si pueden ejecutarse en paralelo pero no tienen acceso a las mismas variables, lo cual limita lo que se puede hacer con guilds pero a la vez ofrece garantías para evitar problemas comunes de programas concurrentes como data races. Para compartir información entre diferentes guilds, se pueden usar channels (muy parecido al modelo de concurrencia en Go) y también se puede compartir objetos que sean inmutables. Para conocer mas sobre este modelo de concurrencia te recomiendo leer este blog post (https://mensfeld.pl/2016/11/getting-ready-for-new-concurrency-in-ruby-3-with-guilds/) en donde explican a grandes rasgos como prepararnos para el nuevo modelo de concurrencia en Ruby 3.

**Tipos**

Finalmente, en lo que respecta a tipos en Ruby 3, aunque es uno de los grandes objetivos pues se sabe que la comunidad está pidiendo soporte para esto, el trabajo en este campo no está tan avanzado como en los otros 2 puntos.

**Release de Ruby 3**

En una conversación con 3 líderes de la comunidad incluyendo a Matz (el creador de Ruby), se habló de Ruby 3x3 y de los diferentes avances, sin embargo dijeron que hay todavía mucho trabajo por hacer y qué podemos esperar por Ruby 3 hasta el 2020.
Puedes encontrar la conversación en https://blog.heroku.com/ruby-3-by-3 y un post en redit en donde se hace un resumen del avance de Ruby 3x3 en https://www.reddit.com/r/ruby/comments/7hhgrt/where_is_the_work_on_ruby_3/

Se espera para diciembre de 2020 el release de Ruby 3, con actualizaciones importantes en:

- Rendimiento
- Concurrencia
- Análisis estático
- Problemas de transición

![img](https://sloboda-studio.com/wp-content/uploads/2020/05/Screenshot-2020-05-27T121358.504.png)

Más información en [Big News: Ruby Version 3.0](https://sloboda-studio.com/blog/big-news-ruby-version-3-0/)

# 3. Proyecto

## Introducción al proyecto

Snake 🐍

[Iniciando con Ruby 2D](http://www.ruby2d.com/learn/get-started/)

```ruby
require 'ruby2d'
```

## Instalar Ruby 2D

Puedes encontrar las instrucciones de instalación de Ruby2D para diferente plataforma en la página oficial de Ruby2D (http://www.ruby2d.com/learn/get-started/), sin embargo acá te dejo
un resumen.

Ruby2D depende de una libreria nativa llamada simple2D (https://github.com/simple2d/simple2d) que debemos instalar para poder utilizar la gema Ruby2D exitosamente.

Para instalar esta librería sigue estas instrucciones:

### MacOS

```sh
brew tap simple2d/tap`
`brew install simple2d
```

### Windows

En MingW ejecuta el siguiente comando

url=‘https://raw.githubusercontent.com/simple2d/simple2d/master/bin/simple2d.sh’; which curl > /dev/null && cmd=‘curl -fsSL’ || cmd=‘wget -qO -’; bash <($cmd $url) install

Alternativamente sigue las instrucciones de instalación de Ruby2D que encuentras en http://www.ruby2d.com/learn/windows/

## Overview Ruby 2D

### Linux

Sigue las instrucciones en http://www.ruby2d.com/learn/linux/ dependiendo de tu distribución de Linux.

Instalar rbenv

```sh
rbenv install 3.0.1
rbenv global 3.0.1
```

Instalar dependencias

```sh
sudo pacman -S sdl2 sdl2_image sdl2_mixer sdl2_ttf
```

Instalar 2d

```ruby
gem install ruby2d
```

Ejecutar nuestro archivo

```ah
bundle exec ruby snake.rb
```

Agregar a configuracion

```sh
 # ruby rbenv
 eval "$(rbenv init - zsh)"
    
 export PATH="$HOME/.rbenv/bin:$PATH"
 eval "$(rbenv init -)"
```

[Ruby 2D on Linux](https://www.ruby2d.com/learn/linux/)



## Arquitectura

![arquitecturaruby.jpeg](https://static.platzi.com/media/public/uploads/arquitecturaruby_18186e3a-e306-483c-9f0a-62badef29b2f.jpeg)

![arquitecturaruby2.jpeg](https://static.platzi.com/media/public/uploads/arquitecturaruby2_2df8cafe-fcd5-4b05-a928-4e2d07bb85a6.jpeg)

> Excelente ejercicio… Tomarnos un tiempo para definir una ruta de trabajo antes de comenzar a codear. Se que es lo que siempre debemos hacer pero por lo general en las clases solo se informa de manera verbal.

El usuario, mediante Ruby2D hará acciones, a su vez estas acciones modificarán el estado de la aplicación y esto será notificado a la vista para que se actualice.

Componentes de la aplicación:

- Grid
- Serpiente
- Comida

## Estructurando el proyecto

Si ejecutan ese código les generará un error, porque al usar Struct tenemos que definir las variables de la clase como símbolos.

```ruby
#Representación del Estado de la aplicación

module Model
    class Coord < Struct.new(:row, :col)
    end

    class Food < Coord
    end

    class Snake < Struct.new(:positions)
    end


    class Grid < Struct.new(:rows, :cols)
    end
end
```

- [Understanding Redux store concepts by little Ruby reimplementation - Josef Strzibny](https://medium.com/cloudaper/understanding-redux-store-concepts-by-little-ruby-reimplementation-d08bcc05dee8)

## Vista básica

Código del archivo: app.rb

```ruby
# Nos ayuda a incluir archivo a este archivo, como @import de sass
require_relative "view/ruby2d"
require_relative "model/state"

vista = View::Ruby2dView.new

estado_inicial = Modelo::initial_state
vista.render(estado_inicial)
```

Código del archivo: state.rb

```ruby
# Representacion del estado del juego

module Modelo
    class Coord < Struct.new(:row,:col)
    end

    class Food < Coord
    end

    class Snake < Struct.new(:positions)
    end

    class Grid < Struct.new(:rows,:cols)
    end

    class State < Struct.new(:Snake, :Food, :Grid)
    end

    def self.initial_state
        Modelo::State.new(
            Modelo::Snake.new([
                Modelo::Coord.new(1,1),
                Modelo::Coord.new(0,1),
            ]),
            Modelo::Food.new(4,4),
            Modelo::Grid.new(10,12)
        )
    end
end
```

Código del archivo: ruby2d.rb

```ruby
require 'ruby2d'

module View
    class Ruby2dView
        def initialize
            @pixel_size = 50
        end

        def render(state)
            extend Ruby2D::DSL
            set(
                title: "La culebrita",
                width: @pixel_size * state.Grid.cols,
                height: @pixel_size * state.Grid.rows
            )
            render_snake(state)
            render_food(state)
            show
        end

        private
        def render_snake(state)
            extend Ruby2D::DSL
            serpiente = state.Snake
            serpiente.positions.each do |pos|
                Square.new(
                    x: pos.col * @pixel_size,
                    y: pos.row * @pixel_size,
                    size: @pixel_size,
                    color: '#00b7c2'
                )
            end
        end

        def render_food(state)
            extend Ruby2D::DSL
            comida = state.Food
            Square.new(
                x: comida.col * @pixel_size,
                y: comida.row * @pixel_size,
                size: @pixel_size,
                color: '#ecfeff'
            )
        end
    end
end
```

## Programando la base para los movimientos

`Model::Direction::RIGHT`, pero desde fuera si es obligatorio, igual al parecer es una buena practica llamarlo completo.

Todo depende del SCOPE se llama:

Por fuera del modulo:
`Model::Direction::RIGHT`

Por dentro del modulo:
`Direction::RIGHT`

`actions.rb`

```ruby
module Actions
  def self.move_snake(state)
    next_direction = state.next_direction
    next_position = calc_next_position(state)
    # verificar que la siguiente casilla se valida
    if next_position_is_valid?(state, next_position)
      move_snake_to(state, next_position)
    else
    end_game(state)
    end

    private
    
    def calc_next_position(state)
      curr_position = state.snake.positions.first
      case state.next_direction
      when UP
        # decrementar fila
        # [(1,1), (0,1)]
        return = Model::Coord.new(
          curr_position.row - 1, 
          curr_position.row)
      when RIGHT
        # incrementar col
        return Model::Coord.new(
          curr_position.row,
          curr_position.col + 1
        )
      when DOWN
        # incrementar fila
        return Model::Coord.new(
          curr_position.row + 1,
          curr_position.col
        )
      when LEFT
        # decrementar col
        return Model::Coord.new(
          curr_position.row,
          curr_position.col - 1
        )
      end
    end


    # Si no es valida -> terminar el juevo
    # Si es validad -> movemos la serpiente
  end
end
```

## Programando el movimiento de nuestro snake

El rango utilizado en el array de “new_position” no tiene en cuenta la última posición del array (que vendría a ser la cola actual de la serpiente).

```ruby
new_positions = [next_position] + state.snake.positions[0...-1]
```

El resultado sería que la cabeza (antes de cambiar la posición) ahora es la cola! Agregada obviamente a la nueva posición.

```ruby
state.snake.positions[0...-1]
```

Esta parte el código generalmente la tendemos a conocer utilizando métodos para borrar la última posición, pero creo que esta forma además de ser más elegante (porque se ve muy pro), tiene un poder inmenso, porque por ejemplo si tenemos un array de 10 números podríamos borrar 9 en un solo paso!

## Test de nuestras acciones

**test/actions_test.rb**

```ruby
require "minitest/autorun"
require_relative "../src/actions/actions"
require_relative "../src/model/state"

class ActonsTest < Minitest::Test
  
  def test_move_snake
    initial_state = Model::State.new(
      Model::Snake.new([
        Model::Coord.new(1,1),
        Model::Coord.new(0,1)
      ]),
      Model::Food.new(4, 4),
      Model::Grid.new(8, 12),
      Model::Direction::DOWN,
      false
    )

    expected_state = initial_state = Model::State.new(
      Model::Snake.new([
        Model::Coord.new(2,1),
        Model::Coord.new(1,1)
      ]),
      Model::Food.new(4, 4),
      Model::Grid.new(8, 12),
      Model::Direction::DOWN,
      false
    )
    
    actual_state = Actions::move_snake(initial_state)
    assert_equal actual_state, expected_state
  end
end
```

**Ejecutando test**

```ruby
bundle exec ruby test/actions_test.rb
```

**Proceso finalizado**

```sh
ruby-on-rails/ruby/snake on  main [!?] via 💎 v3.0.1
❯ bundle exec ruby test/actions_test.rb
Run options: --seed 19873

# Running:

.

Finished in 0.001426s, 701.2726 runs/s, 701.2726 assertions/s.

1 runs, 1 assertions, 0 failures, 0 errors, 0 skips
```

## Implementando el timer del movimiento

**app.rb**

```ruby
require_relative "view/ruby2d"
require_relative "model/state"
require_relative "actions/actions"

class App
  def initialize
    @state = Model::initial_state
  end

  def start
    view = View::Ruby2dView.new
    Thread.new { init_timer(view) }
    view.start(@state)
  end

  def init_timer(view)
    loop do
      @state = Actions::move_snake(@state)
      view.render(@state)
      sleep 0.5
    end
  end
end

app = App.new
app.start
```



>  este lenguaje por eso toma con cautela lo que creo que puede estar pasando. como creo que estas usando una version parecida a la mía de la gema Ruby2D(gem “ruby2d”, “~> 0.10.0”). Problema: La librería “dsl.rb” tiene un método “render” lo puedes ver en el github del proyecto Ruby2D, entonces el ruby está llamando este método y no el que estamos implementando.
> Solucion: cambia el nombre del método render a “renderGame” o el que tu quieras y seguro te va a funcionar!!!

## Actualizando la vista

**ruby2d.rb**

```ruby
    private

    def render_food(state)
      @food.remove if @food
      extend Ruby2D::DSL
      food = state.food
      @food = Square.new(
        x: food.col * @pixel_size,
        y: food.row * @pixel_size,
        size: @pixel_size,
        color: 'yellow'
      )
    end

    def render_snake(state)
      @snake_positions.each(&:remove) if @snake_positions
      extend Ruby2D::DSL
      snake = state.snake
      @snake_positions = snake.positions.map do |pos|
        Square.new(
          x: pos.col * @pixel_size,
          y: pos.row * @pixel_size,
          size: @pixel_size,
          color: 'green'
        )
      end
    end
```

**bpaste** para publicar codigo.

[Usar Bundler para instalar Ruby gems](https://help.dreamhost.com/hc/es/articles/115001070131-Usar-Bundler-para-instalar-Ruby-gems)

## Preparando el cambio de dirección

`actions.rb`

```ruby
  def self.next_direction_is_valid?(state, direction)
    case state.curr_direction
    when Model::Direction::UP
      return true if direction != Model::Direction::DOWN
    when Model::Direction::DOWN
      return true if direction != Model::Direction::UP
    when Model::Direction::RIGHT
      return true if direction != Model::Direction::LEFT
    when Model::Direction::LEFT
      return true if direction != Model::Direction::RIGHT
    end  
    
    return false
  end
```



> La ‘@’ antes de la variable, la define como variable de instancia.

🚀

```sh
Run options: --seed 18412

# Running:

Invalid direction
...

Finished in 0.001023s, 2932.0268 runs/s, 2932.0268 assertions/s.

3 runs, 3 assertions, 0 failures, 0 errors, 0 skips
```

## Recapitulación de la arquitectura

Nuestro proyecto tiene 5 componentes principales:

- App: Es el punto de entrada al proyecto
- Ruby2D: Es la vista que implementamos utilizando Ruby2D
- Timer: Está dentro de la aplicación, es el que envía el movimiento de la serpiente
- Actions: Es donde está la lógica que es capaz de modificar al estado
- State: Es el estado de nuestra aplicación

```sh
🚀 🚀 🚀 🚀 Genial 🚀 🚀 🚀 🚀
```



## Moviendo la serpiente con el teclado

 condición que se coloco para saber si un estado (state) cambio verificando su respectivo hash, en el código quedo:

```ruby
if new_state.hash != @state
  @state = new_state
  ...
end
```

Deberíamos compararlo con el hash de @state porque si no siempre se esta renderizando la vista:

```ruby
if new_state.hash != @state.hash
  @state = new_state
  ...
end
```

[simon019 platzi-curso-ruby](https://github.com/simon0191/platzi-curso-ruby/blob/master/snake/src/app.rb#L33)

## Creciendo la serpiente y programando la condición de derrota

`actions.rb`

```ruby
...  
def self.position_is_food?(state, next_position)
    state.food.row == next_position.row && state.food.col == next_position.col
  end

  def self.grow_snake_to(state, next_position)
    new_positions = [next_position] + state.snake.positions
    state.snake.positions = new_positions
    state
  end
```

`test_action.rb`

```ruby
...
  def test_change_direction_valid
    expected_state = Model::State.new(
      Model::Snake.new([
        Model::Coord.new(1,1),
        Model::Coord.new(0,1)
      ]),
      Model::Food.new(4, 4),
      Model::Grid.new(8, 12),
      Model::Direction::LEFT,
      false
    )

    actual_state = Actions::change_direction(@initial_state, Model::Direction::LEFT)
    assert_equal  actual_state, expected_state
  end

  def test_snake_grow
    initial_state = Model::State.new(
      Model::Snake.new([
        Model::Coord.new(1,1),
        Model::Coord.new(0,1)
      ]),
      Model::Food.new(2, 1),
      Model::Grid.new(8, 12),
      Model::Direction::DOWN,
      false
    )
    
    actual_state = Actions::move_snake(initial_state)
    assert_equal(actual_state.snake.positions, [
      Model::Coord.new(2,1),
      Model::Coord.new(1,1),
      Model::Coord.new(0,1)
    ])
  end
```



## Generando comida aleatoria: uso de rand y stub en pruebas

`actions.rb`

```ruby
  def self.generate_food(state)
    new_food = Model::Food.new(rand(state.grid.rows), rand(state.grid.cols))
    state.food = new_food
    state
  end
```

`test_actions.rb`

```ruby
def test_generate_food
    initial_state = Model::State.new(
      Model::Snake.new([
        Model::Coord.new(1,1),
        Model::Coord.new(0,1)
      ]),
      Model::Food.new(2, 1),
      Model::Grid.new(8, 12),
      Model::Direction::DOWN,
      false
    )
    
    expected_state = Model::State.new(
      Model::Snake.new([
        Model::Coord.new(2,1),
        Model::Coord.new(1,1),
        Model::Coord.new(0,1)
      ]),
      Model::Food.new(0, 0),
      Model::Grid.new(8, 12),
      Model::Direction::DOWN,
      false
    )
    
    Actions.stub(:rand, 0) do
      actual_state = Actions::move_snake(initial_state)
      assert_equal actual_state, expected_state
    end
  end

```

## Condición de salida y conclusiones

`ruby2d.rb`

```ruby
....    
	def renderGame(state)
      extend Ruby2D::DSL
      close if state.game_finished
      render_food(state)
      render_snake(state)
    end
...
```

`app.rb`

```ruby
  def init_timer
    loop do
      if @state.game_finished
        puts "Juego Finalizado"
        puts "Puntaje: #{@state.snake.positions.length}"
        break
      end
      @state = Actions::move_snake(@state)
      # Trigger movement
      @view.renderGame(@state)
      sleep 0.5
    end
  end
```

## Retos del curso

### **[simon0191/platzi-curso-ruby](https://github.com/simon0191/platzi-curso-ruby)**