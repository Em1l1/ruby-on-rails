# Primera forma
# Ciclos
# x = 1
# while x < 5 do
#   puts "hola #{x}"
#   x += 1
# end

# segunda forma

# x = 1
# loop do 
#   puts "hola #{x}"
#   break if x >= 5
#   x += 1
# end


# Tercera forma 

# for i in 1..10 do
#   puts "hola #{i}"
# end

# [1,2,3,4].each { |x| puts "hola #{x}" }

8.times { |x| puts "hola     #{x}" }

