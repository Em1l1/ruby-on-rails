friends = ["Kevin", "Karen", "Oscar", "Angela", "Mike"]

# for
# for e in friends
#   puts e
# end

# each
# friends.each do |friend|
#   puts friend
# end

# for index in 0..5
#   puts index
# end

6.times do |index|
  puts index
end
